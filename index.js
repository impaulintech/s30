const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 3001;

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.listen(port, () => { console.log(`server is running at port${port}`) })

mongoose.connect('mongodb+srv://paulintech:asd123@s30.ra1ml.mongodb.net/test?authSource=admin&replicaSet=atlas-ojl7my-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true',
{
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

let db = mongoose.connection;

db.on("error",console.error.bind(console, "connection error"));
db.once("open", () => { console.log("We're connected to the cloud database") })

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

const Task = mongoose.model("Task", taskSchema)

app.post('/tasks', (req, res) => {
    Task.findOne({ name: req.body.name }, (err, result) => {
        
        if (result != null && result.name == req.body.name) {
            return res.send('Duplicate task found')
        } else {

            let newTask = new Task({
                name: req.body.name
            })

            newTask.save((saveErr, savedTask) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send('New task created')
                }
            }) 

        }

    });
})